### Name

The name of your component.

### Goal

What this component will acomplish.

### Tasks

List the small tasks that building this component will
require. Below there is a standard list.

* [ ] Write the template
* [ ] Define the props
* [ ] Create the style
* [ ] Write its documentation
