/* tslint:disable:no-any */
import Vue from 'vue';
import VueApollo from 'vue-apollo';

import { ApolloClient } from 'apollo-client';
import { ApolloLink, GraphQLRequest } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';

import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';

const cache: InMemoryCache = new InMemoryCache();

const httpLink: HttpLink = new HttpLink({
  uri: 'http://localhost:3000/graphql',
});

const authLink: ApolloLink = setContext(
  (_: GraphQLRequest, { headers }: any): ApolloLink => {
    const token: string | null = localStorage.getItem('token');
    return {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    };
  },
);

const link: ApolloLink = ApolloLink.from([authLink, httpLink]);

const apolloClient: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
});

export default new VueApollo({
  defaultClient: apolloClient,
});

Vue.use(VueApollo);
