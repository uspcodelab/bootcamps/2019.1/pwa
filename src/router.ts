/* tslint:disable:typedef */

import Vue, { VueConstructor } from 'vue';
import Router from 'vue-router';
import { createRouterLayout } from 'vue-router-layout';

const RouterLayout: VueConstructor<Vue> = createRouterLayout(
  (layout: string) => {
    return import('@/layouts/' + layout + '.vue');
  },
);

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: RouterLayout,
      children: [
        {
          path: '',
          component: () => import('@/pages/Home.vue'),
        },
        {
          path: 'components',
          component: () => import('@/pages/ComponentList.vue'),
        },
        {
          path: 'form',
          component: () => import('@/pages/RegistrationForm.vue'),
        },
        {
          path: '/h/hackathon',
          component: () => import('@/pages/HackathonForm.vue'),
        },
        {
          path: '/h/hackathon/ed',
          component: () => import('@/pages/EditionForm.vue'),
        },
      ],
    },
  ],
});
