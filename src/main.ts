/* tslint:disable:no-any */
import Vue, { CreateElement, VNode } from 'vue';

import App from './App.vue';

import router from './router';
import apolloProvider from './vue-apollo';

import './registerServiceWorker';

Vue.config.productionTip = false;

const requiredComponents: __WebpackModuleApi.RequireContext = require.context(
  './components/Base',
  false,
  /[A-Z]\w+\.vue$/,
);

requiredComponents.keys().forEach((fileName: string) => {
  const componentConfig: any = requiredComponents(fileName);
  const componentName: string = fileName.replace(/^\.\/([A-Z].*)\.vue$/, '$1');
  Vue.component(componentName, componentConfig.default || componentConfig);
});

new Vue({
  router,
  apolloProvider,
  render: (h: CreateElement): VNode => h(App),
}).$mount('#app');
